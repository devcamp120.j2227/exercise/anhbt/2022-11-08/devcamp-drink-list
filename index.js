// import lib express
const express = require("express");

// init app express
const app = express();

// declare port
const port = 8000;

class Drink {
    constructor(id, code, name, price, dateCreated, dateUpdated) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.price = price;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
    }
}

var drinkList = [
    new Drink (
        1,
        "TRATAC",
        "Trà tắc",
        10000,
        "14/5/2021",
        "14/5/2021"
    ),
    new Drink (
        2,
        "COCA",
        "Cocacola",
        15000,
        "14/5/2021",
        "14/5/2021"
    ),
    new Drink (
        3,
        "PEPSI",
        "Pepsi",
        15000,
        "14/5/2021",
        "14/5/2021"
    )
]

app.get("/drinks-class", (request, response) => {
    const query = request.query;

    const code = query.code;
    if (Object.keys(query).length == false || code == false) {
        response.json({drinkList : drinkList});
    } else {
        response.json({drinkList : drinkList.filter(drink => drink.code == code)});
    }
})

app.get("/drinks-object", (request, response) => {
    const query = request.query;

    const code = query.code;
    if (Object.keys(query).length == false || code == false) {
        response.json({drinkList : drinkList});
    } else {
        response.json({drinkList : drinkList.filter(drink => drink.code == code)});
    }
})
app.get("/drinks-class/drinkId", (request, response) => {
    const query = request.query;

    const id = query.id;
    if (Object.keys(query).length == false || code == false) {
        response.json({drinkList : drinkList});
    } else {
        response.json({drinkList : drinkList.filter(drink => drink.id == id)});
    }
})

app.get("/drinks-object/drinkId", (request, response) => {
    const query = request.query;

    const id = query.id;
    if (Object.keys(query).length == false || code == false) {
        response.json({drinkList : drinkList});
    } else {
        response.json({drinkList : drinkList.filter(drink => drink.id == id)});
    }
})

// run app on declared port
app.listen(port, () => {
    console.log(`App running on port ${port}`);
})